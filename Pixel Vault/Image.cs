﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Pixel_Vault
{
    public class Image
    {
        string path;
        public int width, height;
        public byte[,,] img;
        public byte[] pxl;

        public Image(string pth)
        {
            path = pth;
            Bitmap bm_img = new Bitmap(path);
            //Obtener imagen
            width = bm_img.Width;
            height = bm_img.Height;
            img = new byte[width, height, 3];
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    img[w, h, 0] = bm_img.GetPixel(w, h).R;
                    img[w, h, 1] = bm_img.GetPixel(w, h).G;
                    img[w, h, 2] = bm_img.GetPixel(w, h).B;
                }
            }
            bm_img.Dispose();
        }

        public void Dim3to1()
        {
            int index = -1;
            pxl = new byte[width * height * 3];
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    index++; pxl[index] = img[w, h, 0];
                    index++; pxl[index] = img[w, h, 1];
                    index++; pxl[index] = img[w, h, 2];
                }
            }
        }

        public void Dim1to3()
        {
            int x = -1, y = 0;
            img = new byte[width, height, 3];
            for (int i = 0; i < pxl.Length; i+=3)
            {
                x++;
                img[x, y, 0] = pxl[i];
                img[x, y, 1] = pxl[i + 1];
                img[x, y, 2] = pxl[i + 2];
                if (x == width - 1)
                {
                    x = -1; y++;
                }
            }
        }

        public void Save()
        {
            Bitmap bmp = new Bitmap(width, height);
            for (int h = 0; h < height; h++)
            {
                for (int w = 0; w < width; w++)
                {
                    bmp.SetPixel(w, h, Color.FromArgb(img[w, h, 0], img[w, h, 1], img[w, h, 2]));
                }
            }            
            bmp.Save(path, System.Drawing.Imaging.ImageFormat.Png);
            bmp.Dispose();
        }
    }
}
