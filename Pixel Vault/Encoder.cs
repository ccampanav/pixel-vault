﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Pixel_Vault
{
    public class Encoder
    {
        Home frmHome;
        int[] passwd;
        Image[] images;
        int n_iterations;

        public Encoder(Form frmParent, string pwd, string[] imgs)
        {
            frmHome = (Home)frmParent;
            frmHome.SetStatus("Inicializando...");
            GetPassword(pwd);
            n_iterations = 7 + passwd.Length;
            images = new Image[imgs.Length];
            for (int z = 0; z < images.Length; z++)
            {
                frmHome.SetStatus("Imagen " + (z + 1) + "/" + images.Length + ". Cargando...");
                images[z] = new Image(imgs[z]);
                Status(z + 1, 1); SpinX(z);
                Status(z + 1, 2); SpinY(z);
                Status(z + 1, 3); SwitchRGB(z);
                Status(z + 1, 4); images[z].Dim3to1(); ReverseA1(z);
                Status(z + 1, 5); SeparatorOP(z);
                Status(z + 1, 6); ReverseParts(z);
                Status(z + 1, 7); AddN(z);
                Status(z + 1, 8); ReverseBits(z);
                for (int a = 0; a < passwd.Length - 1; a++)
                {
                    Status(z + 1, 9 + a); GroupsN(z, passwd[a] * passwd[a + 1]);
                }
                frmHome.SetStatus("Imagen " + (z + 1) + "/" + images.Length + ". Guardando...");
                images[z].Dim1to3();
                images[z].Save();
            }
            frmHome.StatusProcess(false);
        }

        private void Status(int nImg, int nIte)
        {
            frmHome.SetStatus("Imagen " + nImg + "/" + images.Length + ". Cifrado " + nIte + "/" + n_iterations);
        }

        private void GetPassword(string pwd)
        {         
            string chars = "0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ!\"#$%&'()*+,-./:;<=>?@[\\]_´{|}áéíó¿¡";
            passwd = new int[pwd.Length];
            int idx = -1;
            for (int z = 0; z < pwd.Length; z++)
            {
                for (int c = 0; c < chars.Length; c++)
                {
                    if (pwd.Substring(z, 1).Equals(chars.Substring(c, 1)))
                    {
                        idx++;
                        passwd[idx] = c;
                        break;
                    }
                }                
            }
        }

        //Filtros

        private int[] CalcP1P2(int k)
        {
            int[] answ = new int[2];           
            string[] strs = { (k * Math.Sin(k)).ToString(), (k * Math.Cos(k)).ToString() };
            for (int i = 0; i < 2; i++)
            {
                answ[i] = 0;
                for (int c = 0; c < strs[i].Length; c++)
                {
                    if (strs[i].Substring(c, 1).Equals("."))
                    {
                        answ[i] = int.Parse(strs[i].Substring(c + 1, 1));
                        break;
                    }
                }
            }        
            return answ;
        }

        private void SpinX(int index)
        {
            byte[,,] img = new byte[images[index].width, images[index].height, 3];
            int k = passwd[0];
            int[] pp = CalcP1P2(k);
            //Girar matriz R
            if (pp[0] % 2 == 1) 
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 0] = images[index].img[images[index].width - x - 1, y, 0];
                    }
                }
            }
            else
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 0] = images[index].img[x, y, 0];
                    }
                }
            }
            //Girar matriz G
            if (pp[1] % 2 == 1) 
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 1] = images[index].img[images[index].width - x - 1, y, 1];
                    }
                }
            }
            else
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 1] = images[index].img[x, y, 1];
                    }
                }
            }
            //Copiar matriz en B
            for (int x = 0; x < images[index].width; x++)
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    img[x, y, 2] = images[index].img[x, y, 2];
                }
            }
            images[index].img = img;
        }

        private void SpinY(int index)
        {
            byte[,,] img = new byte[images[index].width, images[index].height, 3];
            int k = passwd[1];
            int[] pp = CalcP1P2(k);
            //Copiar matriz en R
            for (int x = 0; x < images[index].width; x++)
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    img[x, y, 0] = images[index].img[x, y, 0];
                }
            }
            //Girar matriz G
            if (pp[0] % 2 == 1) 
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 1] = images[index].img[x, images[index].height - y - 1, 1];
                    }
                }
            }
            else
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 1] = images[index].img[x, y, 1];
                    }
                }
            }
            //Girar matriz B
            if (pp[1] % 2 == 1) 
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 2] = images[index].img[x, images[index].height - y - 1, 2];
                    }
                }
            }
            else
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 2] = images[index].img[x, y, 2];
                    }
                }
            }
            images[index].img = img;
        }

        private void SwitchRGB(int index)
        {
            //Obtener el nuevo orden para RGB
            List<int> k = new List<int>();
            k.Add(passwd[2]); k.Add(passwd[3]); k.Add(passwd[4]);
            k.Sort((a, b) => a.CompareTo(b));
            int pr = 0, pg = 1, pb = 2;
            for (int v = 2; v < 5; v++)
            {
                if (k[0] == passwd[v])
                {
                    pr = v - 2;
                }
                if (k[1] == passwd[v])
                {
                    pg = v - 2;
                }
                if (k[2] == passwd[v])
                {
                    pb = v - 2;
                }
            }
            //Aplicar nuevo orden
            byte[,,] img;
            img = new byte[images[index].width, images[index].height, 3];
            for (int y = 0; y < images[index].height; y++)
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    img[x, y, pr] = images[index].img[x, y, 0];
                    img[x, y, pg] = images[index].img[x, y, 1];
                    img[x, y, pb] = images[index].img[x, y, 2];
                }
            }
            images[index].img = img;
        }

        private void ReverseA1(int index)
        {
            byte[] p;           
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < p.Length; i++)
            {
                p[i] = images[index].pxl[p.Length - i - 1];
            }
            images[index].pxl = p;
        }

        private void SeparatorOP(int index)
        {
            int k = passwd[5];
            List<byte> no, pair, odd;
            byte[] p;
            no = new List<byte>();
            pair = new List<byte>();
            odd = new List<byte>();
            p = new byte[images[index].pxl.Length];
            //Obtener elementos excluidos
            for (int i = 0; i < k; i++)
            {
                no.Add(images[index].pxl[i]);
            }
            //Clasificar elementos de posiciones pares e impares
            for (int i = k; i < p.Length; i++)
            {
                if (i % 2 == 0)
                {
                    pair.Add(images[index].pxl[i]);
                }
                else
                {
                    odd.Add(images[index].pxl[i]);
                }
            }
            //Unificar listas
            int idx = -1;
            if (k % 2 == 1)
            {
                for (int i = 0; i < no.Count; i++)
                {
                    idx++; p[idx] = no[i];
                }
            }
            for (int i = 0; i < pair.Count; i++)
            {
                idx++; p[idx] = pair[i];
            }
            for (int i = 0; i < odd.Count; i++)
            {
                idx++; p[idx] = odd[i];
            }
            if (k % 2 == 0)
            {
                for (int i = 0; i < no.Count; i++)
                {
                    idx++; p[idx] = no[i];
                }
            }
            images[index].pxl = p;
        }

        private void ReverseParts(int index)
        {
            int k = passwd[6];
            int parts = (images[index].pxl.Length - (images[index].pxl.Length % k)) / k;
            byte[] aux = new byte[k];
            byte[] p = new byte[images[index].pxl.Length];
            for (int i = 0; i < parts * k; i += k)
            {
                //Invertir grupos
                for (int x = 0; x < k; x++)
                {
                    aux[x] = images[index].pxl[i + k - x - 1];
                }
                //Agregar al arreglo
                for (int x = 0; x < k; x++)
                {
                    p[i + x] = aux[x];
                }
            }
            images[index].pxl = p;
        }

        private void AddN(int index)
        {
            int k = passwd[7], aux;
            byte[] p;
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < p.Length; i++)
            {
                aux = (images[index].pxl[i] + k) % 256;
                while (aux > 255)
                {
                    aux -= 256;
                }
                p[i] = (byte)aux;
            }
            images[index].pxl = p;
        }

        private void ReverseBits(int index)
        {
            byte[] p;
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < p.Length; i++)
            {
                p[i] = (byte)(255 - images[index].pxl[i]);
            }
            images[index].pxl = p;
        }

        private void GroupsN(int index, int groups)
        {
            int aux;
            byte[] p;           
            if (groups == 0)
            {
                groups = 100;
            }
            List<byte>[] columns = new List<byte>[groups];
            //Clasificar elementos en los N grupos
            for (int g = 0; g < groups; g++)
            {
                columns[g] = new List<byte>();
            }
            aux = -1;
            for (int i = 0; i < images[index].pxl.Length; i++)
            {
                aux++; columns[aux].Add(images[index].pxl[i]);
                if (aux == groups - 1)
                {
                    aux = -1;
                }
            }
            //Reescribir grupos en arreglo
            p = new byte[images[index].pxl.Length];
            aux = -1;
            for (int g = 0; g < groups; g++)
            {
                for (int i = 0; i < columns[g].Count; i++)
                {
                    aux++; p[aux] = columns[g][i];
                }
            }
            images[index].pxl = p;           
        }
    }
}
