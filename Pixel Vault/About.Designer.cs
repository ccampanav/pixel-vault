﻿namespace Pixel_Vault
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this.lbl_title = new System.Windows.Forms.Label();
            this.rtxtb_information = new System.Windows.Forms.RichTextBox();
            this.picbx_poweredby = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_poweredby)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_title
            // 
            this.lbl_title.AutoSize = true;
            this.lbl_title.Font = new System.Drawing.Font("Segoe UI", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.lbl_title.Location = new System.Drawing.Point(72, 5);
            this.lbl_title.Name = "lbl_title";
            this.lbl_title.Size = new System.Drawing.Size(248, 65);
            this.lbl_title.TabIndex = 3;
            this.lbl_title.Text = "Pixel Vault";
            // 
            // rtxtb_information
            // 
            this.rtxtb_information.BackColor = System.Drawing.Color.White;
            this.rtxtb_information.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.rtxtb_information.Cursor = System.Windows.Forms.Cursors.Default;
            this.rtxtb_information.DetectUrls = false;
            this.rtxtb_information.ForeColor = System.Drawing.Color.DimGray;
            this.rtxtb_information.Location = new System.Drawing.Point(33, 85);
            this.rtxtb_information.Name = "rtxtb_information";
            this.rtxtb_information.ReadOnly = true;
            this.rtxtb_information.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
            this.rtxtb_information.Size = new System.Drawing.Size(320, 110);
            this.rtxtb_information.TabIndex = 5;
            this.rtxtb_information.Text = "Pixel Vault es un software capaz de cifrar y decifrar imágenes a partir de una co" +
    "ntraseña.\n\nVersión 1.0";
            // 
            // picbx_poweredby
            // 
            this.picbx_poweredby.Image = global::Pixel_Vault.Properties.Resources.PoweredBy;
            this.picbx_poweredby.Location = new System.Drawing.Point(72, 215);
            this.picbx_poweredby.Name = "picbx_poweredby";
            this.picbx_poweredby.Size = new System.Drawing.Size(243, 80);
            this.picbx_poweredby.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_poweredby.TabIndex = 6;
            this.picbx_poweredby.TabStop = false;
            // 
            // About
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(384, 321);
            this.Controls.Add(this.picbx_poweredby);
            this.Controls.Add(this.rtxtb_information);
            this.Controls.Add(this.lbl_title);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximumSize = new System.Drawing.Size(400, 360);
            this.MinimumSize = new System.Drawing.Size(400, 360);
            this.Name = "About";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pixel Vault";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.About_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.picbx_poweredby)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_title;
        private System.Windows.Forms.RichTextBox rtxtb_information;
        private System.Windows.Forms.PictureBox picbx_poweredby;
    }
}