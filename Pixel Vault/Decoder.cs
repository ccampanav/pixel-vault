﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace Pixel_Vault
{
    public class Decoder
    {
        Home frmHome;
        int[] passwd;
        Image[] images;
        int n_iterations, now_iteration;

        public Decoder(Form frmParent, string pwd, string[] imgs)
        {
            frmHome = (Home)frmParent;
            frmHome.SetStatus("Inicializando...");
            GetPassword(pwd);
            n_iterations = 7 + passwd.Length;
            images = new Image[imgs.Length];
            now_iteration = 0;
            for (int z = 0; z < images.Length; z++)
            {
                frmHome.SetStatus("Imagen " + (z + 1) + "/" + images.Length + ". Cargando...");
                images[z] = new Image(imgs[z]);
                images[z].Dim3to1();
                for (int a = passwd.Length - 1; a > 0; a--)
                {
                    Status(z + 1); GroupsN(z, passwd[a] * passwd[a - 1]);
                }
                Status(z + 1); ReverseBits(z);
                Status(z + 1); AddN(z);
                Status(z + 1); ReverseParts(z);
                Status(z + 1); SeparatorOP(z);
                Status(z + 1); ReverseA1(z);
                Status(z + 1); images[z].Dim1to3(); SwitchRGB(z);
                Status(z + 1); SpinY(z);
                Status(z + 1); SpinX(z);
                frmHome.SetStatus("Imagen " + (z + 1) + "/" + images.Length + ". Guardando...");
                images[z].Save();
            }
            frmHome.StatusProcess(false);
        }

        private void Status(int nImg)
        {
            now_iteration++;
            frmHome.SetStatus("Imagen " + nImg + "/" + images.Length + ". Cifrado " + now_iteration + "/" + n_iterations);
        }

        private void GetPassword(string pwd)
        {
            string chars = "0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ!\"#$%&'()*+,-./:;<=>?@[\\]_´{|}áéíó¿¡";
            passwd = new int[pwd.Length];
            int idx = -1;
            for (int z = 0; z < pwd.Length; z++)
            {
                for (int c = 0; c < chars.Length; c++)
                {
                    if (pwd.Substring(z, 1).Equals(chars.Substring(c, 1)))
                    {
                        idx++;
                        passwd[idx] = c;
                        break;
                    }
                }
            }
        }

        //Filtros

        private void GroupsN(int index, int groups)
        {
            byte[] p;           
            if (groups == 0)
            {
                groups = 100;
            }
            byte[][] columns = new byte[groups][];
            int[] glen = new int[groups];
            int sizeBase = (images[index].pxl.Length - (images[index].pxl.Length % groups)) / groups;
            //Preparar estructuras para clasificación
            for (int g = 0; g < groups; g++)
            {
                glen[g] = sizeBase;
                if (g < images[index].pxl.Length % groups)
                {
                    glen[g]++;
                }
                columns[g] = new byte[glen[g]];
            }
            int[] idxs = new int[groups];
            for (int i = 0; i < groups; i++)
            {
                idxs[i] = -1;
            }
            //Separar por grupos
            int ini = 0, end, aux = -1;
            for (int g = 0; g < groups; g++)
            {
                end = ini + glen[g]; aux++;
                for (int i = ini; i < end; i++)
                {
                    idxs[aux]++; columns[aux][idxs[aux]] = images[index].pxl[i];
                }
                ini = end;
            }
            //Concatenar listas
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < groups; i++)
            {
                idxs[i] = -1;
            }
            aux = 0;
            for (int r = 0; r < images[index].pxl.Length; r++)
            {
                if (idxs[aux] < glen[aux] - 1)
                {
                    idxs[aux]++;  p[r] = columns[aux][idxs[aux]];
                }
                if (aux == groups - 1)
                {
                    aux = 0;
                }
                else
                {
                    aux++;
                }
            }
            //Reescribir grupos en arreglo
            images[index].pxl = p;
        }

        private void ReverseBits(int index)
        {
            byte[] p;
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < p.Length; i++)
            {
                p[i] = (byte)(255 - images[index].pxl[i]);
            }
            images[index].pxl = p;
        }

        private void AddN(int index)
        {
            int k = passwd[7], aux;
            byte[] p;
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < p.Length; i++)
            {
                aux = 256 + ((images[index].pxl[i] - k) % 256);
                while (aux > 0)
                {
                    aux -= 256;
                }
                p[i] = (byte)aux;
            }
            images[index].pxl = p;
        }

        private void ReverseParts(int index)
        {
            int k = passwd[6];
            int parts = (images[index].pxl.Length - (images[index].pxl.Length % k)) / k;
            byte[] aux = new byte[k];
            byte[] p = new byte[images[index].pxl.Length];
            for (int i = 0; i < parts * k; i += k)
            {
                //Invertir grupos
                for (int x = 0; x < k; x++)
                {
                    aux[x] = images[index].pxl[i + k - x - 1];
                }
                //Agregar al arreglo
                for (int x = 0; x < k; x++)
                {
                    p[i + x] = aux[x];
                }
            }
            images[index].pxl = p;
        }

        private void SeparatorOP(int index)
        {
            int k = passwd[5];
            List<byte> no, pair, odd;
            byte[] p;
            no = new List<byte>();
            pair = new List<byte>();
            odd = new List<byte>();            
            //Obtener elementos excluidos
            if (k % 2 == 0)
            {
                for (int i = images[index].pxl.Length - k; i < images[index].pxl.Length; i++)
                {
                    no.Add(images[index].pxl[i]);
                }
            }
            else
            {
                for (int i = 0; i < k; i++)
                {
                    no.Add(images[index].pxl[i]);
                }
            }
            //Clasificar elementos de posiciones pares e impares
            int lenP = 0, lenO = 0;
            if (k % 2 == 0)
            {
                for (int i = 0; i < images[index].pxl.Length - k; i++)
                {
                    if (i % 2 == 0)
                    {
                        lenP++;
                    }
                    else
                    {
                        lenO++;
                    }
                }
                for (int i = 0; i < lenP; i++)
                {
                    pair.Add(images[index].pxl[i]);
                }
                for (int i = lenP; i < images[index].pxl.Length - k; i++)
                {
                    odd.Add(images[index].pxl[i]);
                }
            }
            else
            {
                for (int i = k; i < images[index].pxl.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        lenP++;
                    }
                    else
                    {
                        lenO++;
                    }
                }
                for (int i = k; i < k + lenP; i++)
                {
                    pair.Add(images[index].pxl[i]);
                }
                for (int i = k + lenP; i < images[index].pxl.Length; i++)
                {
                    odd.Add(images[index].pxl[i]);
                }
            }
            //Unificar listas
            int idx = -1;
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < no.Count; i++)
            {
                idx++; p[idx] = no[i];
            }
            lenP = -1; lenO = -1;
            for (int i = k; i < images[index].pxl.Length; i++)
            {
                if (i % 2 == 0)
                {
                    lenP++; idx++; p[idx] = pair[lenP];
                }
                else
                {
                    lenO++; idx++; p[idx] = odd[lenO];
                }
            }
            images[index].pxl = p;
        }

        private void ReverseA1(int index)
        {
            byte[] p;
            p = new byte[images[index].pxl.Length];
            for (int i = 0; i < p.Length; i++)
            {
                p[i] = images[index].pxl[p.Length - i - 1];
            }
            images[index].pxl = p;
        }

        private void SwitchRGB(int index)
        {
            //Obtener el nuevo orden para RGB
            List<int> k = new List<int>();
            k.Add(passwd[2]); k.Add(passwd[3]); k.Add(passwd[4]);
            k.Sort((a, b) => a.CompareTo(b));
            int pr = 0, pg = 1, pb = 2;
            for (int v = 2; v < 5; v++)
            {
                if (k[0] == passwd[v])
                {
                    pr = v - 2;
                }
                if (k[1] == passwd[v])
                {
                    pg = v - 2;
                }
                if (k[2] == passwd[v])
                {
                    pb = v - 2;
                }
            }
            //Aplicar nuevo orden
            byte[,,] img;
            img = new byte[images[index].width, images[index].height, 3];
            for (int y = 0; y < images[index].height; y++)
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    img[x, y, 0] = images[index].img[x, y, pr];
                    img[x, y, 1] = images[index].img[x, y, pg];
                    img[x, y, 2] = images[index].img[x, y, pb];
                }
            }
            images[index].img = img;
        }

        private int[] CalcP1P2(int k)
        {
            int[] answ = new int[2];
            string[] strs = { (k * Math.Sin(k)).ToString(), (k * Math.Cos(k)).ToString() };
            for (int i = 0; i < 2; i++)
            {
                answ[i] = 0;
                for (int c = 0; c < strs[i].Length; c++)
                {
                    if (strs[i].Substring(c, 1).Equals("."))
                    {
                        answ[i] = int.Parse(strs[i].Substring(c + 1, 1));
                        break;
                    }
                }
            }
            return answ;
        }

        private void SpinY(int index)
        {
            byte[,,] img = new byte[images[index].width, images[index].height, 3];
            int k = passwd[1];
            int[] pp = CalcP1P2(k);
            //Copiar matriz en R
            for (int x = 0; x < images[index].width; x++)
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    img[x, y, 0] = images[index].img[x, y, 0];
                }
            }
            //Girar matriz G
            if (pp[0] % 2 == 1) 
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 1] = images[index].img[x, images[index].height - y - 1, 1];
                    }
                }
            }
            else
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 1] = images[index].img[x, y, 1];
                    }
                }
            }
            //Girar matriz B
            if (pp[1] % 2 == 1) 
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 2] = images[index].img[x, images[index].height - y - 1, 2];
                    }
                }
            }
            else
            {
                for (int x = 0; x < images[index].width; x++)
                {
                    for (int y = 0; y < images[index].height; y++)
                    {
                        img[x, y, 2] = images[index].img[x, y, 2];
                    }
                }
            }
            images[index].img = img;
        }

        private void SpinX(int index)
        {
            byte[,,] img = new byte[images[index].width, images[index].height, 3];
            int k = passwd[0];
            int[] pp = CalcP1P2(k);
            //Girar matriz R
            if (pp[0] % 2 == 1) 
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 0] = images[index].img[images[index].width - x - 1, y, 0];
                    }
                }
            }
            else
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 0] = images[index].img[x, y, 0];
                    }
                }
            }
            //Girar matriz G
            if (pp[1] % 2 == 1) 
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 1] = images[index].img[images[index].width - x - 1, y, 1];
                    }
                }
            }
            else
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    for (int x = 0; x < images[index].width; x++)
                    {
                        img[x, y, 1] = images[index].img[x, y, 1];
                    }
                }
            }
            //Copiar matriz en B
            for (int x = 0; x < images[index].width; x++)
            {
                for (int y = 0; y < images[index].height; y++)
                {
                    img[x, y, 2] = images[index].img[x, y, 2];
                }
            }
            images[index].img = img;
        }
    }
}
