﻿namespace Pixel_Vault
{
    partial class Home
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            this.lbl_status = new System.Windows.Forms.Label();
            this.toolt_info = new System.Windows.Forms.ToolTip(this.components);
            this.txtbx_passwd = new System.Windows.Forms.TextBox();
            this.picbx_action = new System.Windows.Forms.PictureBox();
            this.picbx_process = new System.Windows.Forms.PictureBox();
            this.picbx_search = new System.Windows.Forms.PictureBox();
            this.picbx_passwd_len = new System.Windows.Forms.PictureBox();
            this.picbx_information = new System.Windows.Forms.PictureBox();
            this.lbl_images = new System.Windows.Forms.Label();
            this.flpnl_images = new System.Windows.Forms.FlowLayoutPanel();
            this.ofdlg_images = new System.Windows.Forms.OpenFileDialog();
            this.lbl_passwd_help = new System.Windows.Forms.Label();
            this.picbx_passwd = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_action)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_process)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_search)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_passwd_len)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_information)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_passwd)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_status
            // 
            this.lbl_status.ForeColor = System.Drawing.Color.DimGray;
            this.lbl_status.Location = new System.Drawing.Point(279, 522);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(500, 24);
            this.lbl_status.TabIndex = 13;
            this.lbl_status.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // toolt_info
            // 
            this.toolt_info.AutoPopDelay = 5000;
            this.toolt_info.BackColor = System.Drawing.Color.White;
            this.toolt_info.ForeColor = System.Drawing.Color.DimGray;
            this.toolt_info.InitialDelay = 200;
            this.toolt_info.ReshowDelay = 100;
            // 
            // txtbx_passwd
            // 
            this.txtbx_passwd.BackColor = System.Drawing.Color.White;
            this.txtbx_passwd.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtbx_passwd.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbx_passwd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.txtbx_passwd.Location = new System.Drawing.Point(279, 142);
            this.txtbx_passwd.MaxLength = 20;
            this.txtbx_passwd.Name = "txtbx_passwd";
            this.txtbx_passwd.Size = new System.Drawing.Size(202, 32);
            this.txtbx_passwd.TabIndex = 21;
            this.txtbx_passwd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolt_info.SetToolTip(this.txtbx_passwd, "Ingresar contraseña");
            this.txtbx_passwd.UseSystemPasswordChar = true;
            this.txtbx_passwd.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtbx_passwd_MouseClick);
            this.txtbx_passwd.TextChanged += new System.EventHandler(this.txtbx_passwd_TextChanged);
            // 
            // picbx_action
            // 
            this.picbx_action.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_action.Image = global::Pixel_Vault.Properties.Resources.btn_action_1;
            this.picbx_action.Location = new System.Drawing.Point(278, 40);
            this.picbx_action.Name = "picbx_action";
            this.picbx_action.Size = new System.Drawing.Size(230, 50);
            this.picbx_action.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_action.TabIndex = 17;
            this.picbx_action.TabStop = false;
            this.toolt_info.SetToolTip(this.picbx_action, "Cambiar proceso");
            this.picbx_action.Click += new System.EventHandler(this.picbx_action_Click);
            // 
            // picbx_process
            // 
            this.picbx_process.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_process.Image = global::Pixel_Vault.Properties.Resources.btn_process;
            this.picbx_process.Location = new System.Drawing.Point(333, 360);
            this.picbx_process.Name = "picbx_process";
            this.picbx_process.Size = new System.Drawing.Size(120, 120);
            this.picbx_process.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_process.TabIndex = 26;
            this.picbx_process.TabStop = false;
            this.toolt_info.SetToolTip(this.picbx_process, "Procesar imágenes");
            this.picbx_process.Click += new System.EventHandler(this.picbx_process_Click);
            // 
            // picbx_search
            // 
            this.picbx_search.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_search.Image = global::Pixel_Vault.Properties.Resources.btn_search;
            this.picbx_search.Location = new System.Drawing.Point(60, 242);
            this.picbx_search.Name = "picbx_search";
            this.picbx_search.Size = new System.Drawing.Size(60, 60);
            this.picbx_search.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_search.TabIndex = 25;
            this.picbx_search.TabStop = false;
            this.toolt_info.SetToolTip(this.picbx_search, "Seleccionar imágenes");
            this.picbx_search.Click += new System.EventHandler(this.picbx_search_Click);
            // 
            // picbx_passwd_len
            // 
            this.picbx_passwd_len.BackColor = System.Drawing.Color.White;
            this.picbx_passwd_len.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_passwd_len.Location = new System.Drawing.Point(487, 147);
            this.picbx_passwd_len.Name = "picbx_passwd_len";
            this.picbx_passwd_len.Size = new System.Drawing.Size(25, 25);
            this.picbx_passwd_len.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_passwd_len.TabIndex = 22;
            this.picbx_passwd_len.TabStop = false;
            this.toolt_info.SetToolTip(this.picbx_passwd_len, "Mostrar contraseña");
            this.picbx_passwd_len.Click += new System.EventHandler(this.picbx_passwd_len_Click);
            // 
            // picbx_information
            // 
            this.picbx_information.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picbx_information.Image = global::Pixel_Vault.Properties.Resources.btn_info;
            this.picbx_information.Location = new System.Drawing.Point(5, 517);
            this.picbx_information.Name = "picbx_information";
            this.picbx_information.Size = new System.Drawing.Size(30, 30);
            this.picbx_information.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_information.TabIndex = 16;
            this.picbx_information.TabStop = false;
            this.toolt_info.SetToolTip(this.picbx_information, "Acerca del proyecto");
            this.picbx_information.Click += new System.EventHandler(this.picbx_information_Click);
            // 
            // lbl_images
            // 
            this.lbl_images.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_images.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.lbl_images.Location = new System.Drawing.Point(60, 304);
            this.lbl_images.Name = "lbl_images";
            this.lbl_images.Size = new System.Drawing.Size(58, 25);
            this.lbl_images.TabIndex = 24;
            this.lbl_images.Text = "0";
            this.lbl_images.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // flpnl_images
            // 
            this.flpnl_images.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flpnl_images.AutoScroll = true;
            this.flpnl_images.BackColor = System.Drawing.Color.White;
            this.flpnl_images.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flpnl_images.Location = new System.Drawing.Point(126, 230);
            this.flpnl_images.Name = "flpnl_images";
            this.flpnl_images.Size = new System.Drawing.Size(600, 100);
            this.flpnl_images.TabIndex = 27;
            // 
            // ofdlg_images
            // 
            this.ofdlg_images.Filter = "Imagenes | *.png;*.bmp;*.jpg;*.jpeg;*.tif;*.tiff";
            this.ofdlg_images.Multiselect = true;
            this.ofdlg_images.Title = "Selecciona las imágenes a cifrar";
            // 
            // lbl_passwd_help
            // 
            this.lbl_passwd_help.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lbl_passwd_help.Font = new System.Drawing.Font("Segoe UI", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_passwd_help.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(176)))), ((int)(((byte)(240)))));
            this.lbl_passwd_help.Location = new System.Drawing.Point(329, 147);
            this.lbl_passwd_help.Name = "lbl_passwd_help";
            this.lbl_passwd_help.Size = new System.Drawing.Size(100, 25);
            this.lbl_passwd_help.TabIndex = 28;
            this.lbl_passwd_help.Text = "Contraseña";
            this.lbl_passwd_help.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbl_passwd_help.Click += new System.EventHandler(this.lbl_passwd_help_Click);
            // 
            // picbx_passwd
            // 
            this.picbx_passwd.Image = global::Pixel_Vault.Properties.Resources.txtbx_bg;
            this.picbx_passwd.Location = new System.Drawing.Point(268, 135);
            this.picbx_passwd.Name = "picbx_passwd";
            this.picbx_passwd.Size = new System.Drawing.Size(250, 50);
            this.picbx_passwd.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picbx_passwd.TabIndex = 20;
            this.picbx_passwd.TabStop = false;
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = global::Pixel_Vault.Properties.Resources.form_bg;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(784, 551);
            this.Controls.Add(this.lbl_passwd_help);
            this.Controls.Add(this.picbx_action);
            this.Controls.Add(this.flpnl_images);
            this.Controls.Add(this.picbx_process);
            this.Controls.Add(this.picbx_search);
            this.Controls.Add(this.lbl_images);
            this.Controls.Add(this.picbx_passwd_len);
            this.Controls.Add(this.txtbx_passwd);
            this.Controls.Add(this.picbx_passwd);
            this.Controls.Add(this.picbx_information);
            this.Controls.Add(this.lbl_status);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Home";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pixel Vault";
            ((System.ComponentModel.ISupportInitialize)(this.picbx_action)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_process)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_search)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_passwd_len)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_information)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picbx_passwd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_status;
        private System.Windows.Forms.PictureBox picbx_information;
        private System.Windows.Forms.ToolTip toolt_info;
        private System.Windows.Forms.PictureBox picbx_action;
        private System.Windows.Forms.PictureBox picbx_passwd_len;
        private System.Windows.Forms.TextBox txtbx_passwd;
        private System.Windows.Forms.PictureBox picbx_passwd;
        private System.Windows.Forms.PictureBox picbx_process;
        private System.Windows.Forms.PictureBox picbx_search;
        private System.Windows.Forms.Label lbl_images;
        private System.Windows.Forms.FlowLayoutPanel flpnl_images;
        private System.Windows.Forms.OpenFileDialog ofdlg_images;
        private System.Windows.Forms.Label lbl_passwd_help;
    }
}

