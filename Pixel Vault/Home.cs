﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace Pixel_Vault
{
    public partial class Home : Form
    {
        About frmAbout;
        char action;
        string filter_passwd;
        string[] images;
        Thread thread;
        bool is_working;

        public Home()
        {
            InitializeComponent();
            frmAbout = new About();
            is_working = false;
            action = 'c';
            filter_passwd = "0123456789abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ!\"#$%&'()*+,-./:;<=>?@[\\]_´{|}áéíó¿¡";
            images = null;
            CheckForIllegalCrossThreadCalls = false;
            picbx_action.Select();
        }

        private void picbx_information_Click(object sender, EventArgs e)
        {
            frmAbout.Show();
        }

        public void SetStatus(string msg)
        {
            lbl_status.Text = msg;
        }

        private void picbx_action_Click(object sender, EventArgs e)
        {
            if (action == 'c')
            {
                action = 'd';
                picbx_action.Image = Properties.Resources.btn_action_2;
            }
            else
            {
                action = 'c';
                picbx_action.Image = Properties.Resources.btn_action_1;
            }
        }

        private void txtbx_passwd_TextChanged(object sender, EventArgs e)
        {
            string aux = "", ch;
            int txt_len = txtbx_passwd.Text.Length;
            for (int k = 0; k < txt_len; k++)
            {
                for (int f = 0; f < filter_passwd.Length; f++)
                {
                    ch = txtbx_passwd.Text.Substring(k, 1);
                    if (ch.Equals(filter_passwd.Substring(f, 1)) == true)
                    {
                        aux += ch;
                        break;
                    }
                }
            }
            txtbx_passwd.Text = aux;
            txt_len = txtbx_passwd.Text.Length;
            txtbx_passwd.SelectionStart = txt_len;
            switch (txt_len)
            {
                case 0:
                    picbx_passwd_len.Image = null;
                    break;
                case 1:
                    picbx_passwd_len.Image = Properties.Resources.passwd_1;
                    break;
                case 2:
                    picbx_passwd_len.Image = Properties.Resources.passwd_2;
                    break;
                case 3:
                    picbx_passwd_len.Image = Properties.Resources.passwd_3;
                    break;
                case 4:
                    picbx_passwd_len.Image = Properties.Resources.passwd_4;
                    break;
                case 5:
                    picbx_passwd_len.Image = Properties.Resources.passwd_5;
                    break;
                case 6:
                    picbx_passwd_len.Image = Properties.Resources.passwd_6;
                    break;
                case 7:
                    picbx_passwd_len.Image = Properties.Resources.passwd_7;
                    break;
                default:
                    picbx_passwd_len.Image = Properties.Resources.passwd_ok;
                    break;
            }
            lbl_passwd_help.Visible = false;
        }

        private void picbx_passwd_len_Click(object sender, EventArgs e)
        {
            if (txtbx_passwd.UseSystemPasswordChar == true)
            {
                txtbx_passwd.UseSystemPasswordChar = false;
            }
            else
            {
                txtbx_passwd.UseSystemPasswordChar = true;
            }
        }

        private void picbx_search_Click(object sender, EventArgs e)
        {
            if (ofdlg_images.ShowDialog() == DialogResult.OK)
            {
                int len_img = ofdlg_images.FileNames.Length;
                if (len_img > 0)
                {
                    images = ofdlg_images.FileNames;
                    flpnl_images.Controls.Clear();
                    for (int k = 0; k < images.Length; k++)
                    {
                        PictureBox picbx = new PictureBox();
                        picbx.Width = 70; picbx.Height = 70;
                        picbx.SizeMode = PictureBoxSizeMode.Zoom;
                        FileStream fs = new FileStream(images[k], FileMode.Open, FileAccess.Read);
                        picbx.Image = System.Drawing.Image.FromStream(fs);
                        fs.Close();
                        flpnl_images.Controls.Add(picbx);
                    }                
                    lbl_images.Text = len_img.ToString();
                }
            }
        }

        private void picbx_process_Click(object sender, EventArgs e)
        {
            if (is_working == false)
            {
                SetStatus("");
                if (txtbx_passwd.Text.Length >= 8 && txtbx_passwd.Text.Length <= 20)
                {
                    if (images != null && images.Length > 0)
                    {
                        string msg = "¿Seguro que desea ";
                        if (action == 'd')
                        {
                            msg += "de";
                        }
                        msg += "codificar " + images.Length + " imagen(es)?";
                        if (MessageBox.Show(msg, "Pixel Vault", MessageBoxButtons.YesNo) == DialogResult.Yes)
                        {
                            StatusProcess(true);
                        }
                    }
                    else
                    {
                        SetStatus("Selecciona al menos una imagen");
                    }
                }
                else
                {
                    SetStatus("La contraseña debe tener entre 8 a 20 caracteres");
                }
            }           
        }

        public void StatusProcess(bool start)
        {
            if (start == true)
            {
                is_working = true;
                SetStatus("Iniciando...");
                picbx_process.Image = Properties.Resources.spinner;
                picbx_action.Enabled = false;
                txtbx_passwd.Enabled = false;
                picbx_passwd_len.Enabled = false;
                picbx_search.Enabled = false;
                if (action == 'c')
                {
                    thread = new Thread(t => {
                        Encoder encoder = new Encoder(this, txtbx_passwd.Text, images);
                    });
                }
                else
                {
                    thread = new Thread(t => {
                        Decoder encoder = new Decoder(this, txtbx_passwd.Text, images);
                    });
                }                
                thread.IsBackground = true;
                thread.Start();
            }
            else
            {
                thread.Interrupt();
                picbx_action.Enabled = true;
                txtbx_passwd.Enabled = true;
                picbx_passwd_len.Enabled = true;
                picbx_search.Enabled = true;
                picbx_process.Image = Properties.Resources.btn_process;
                SetStatus("Proceso finalizado");
                is_working = false;
            }
        }

        private void lbl_passwd_help_Click(object sender, EventArgs e)
        {
            lbl_passwd_help.Visible = false;
            txtbx_passwd.Select();
        }

        private void txtbx_passwd_MouseClick(object sender, MouseEventArgs e)
        {
            lbl_passwd_help.Visible = false;
        }
    }
}
